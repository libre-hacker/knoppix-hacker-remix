#! /bin/bash
source variables.sh

if [ "0$UID" -ne 0 ]; then
   echo "Error : Only root can run $(basename $0) or use sudo $(basename $0)"; exit 1
fi

# Create new Knoppix ISO
echo "Generate ISO"
#  -allow-limited-size
genisoimage -input-charset ISO-8859-15 -l -r -J -V "KNOPPIX" \
  -b boot/isolinux/isolinux.bin -no-emul-boot -boot-load-size 4 \
  -boot-info-table -c boot/isolinux/boot.cat \
  -o $PFAD/knx/$distrib_name.iso $PFAD/knx/master

echo -e "The new ISO is stored in '"$PFAD"knx/$distrib_name.iso' "

#!/bin/bash
source variables.sh

if [ "0$UID" -ne 0 ]; then
   echo "Error : Only root can run $(basename $0) or use sudo $(basename $0)"; exit 1
fi

# mount iso in $PFAD/isocd
if [[ ! -d $PFAD/isocd ]]
then
    mkdir -p $PFAD/isocd
fi
echo "Mount ISO read only on $PFAD/isocd directory"
mount -o loop $path_iso $PFAD/isocd 2>&1 > /dev/null
echo "ISO content:"
ls $PFAD/isocd/


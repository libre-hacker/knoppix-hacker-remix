#!/bin/bash

# Enable screensaver
knoppix_user=`cat /etc/passwd | grep knoppix`
if [ "${knoppix_user}" ]
then
        su knoppix -c "xscreensaver -nosplash &"
fi


#!/bin/bash
source variables.sh

if [ "0$UID" -ne 0 ]; then
   echo "Error : Only root can run $(basename $0) or use sudo $(basename $0)"; exit 1
fi

for i in dev/pts proc sys dev tmp; do
  umount $PFAD/knx/source/KNOPPIX/$i
done
sed -i '2,$d' $PFAD/knx/source/KNOPPIX/etc/resolv.conf

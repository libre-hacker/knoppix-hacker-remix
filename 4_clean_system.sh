#!/bin/bash
source variables.sh

if [ "0$UID" -ne 0 ]; then
   echo "Error : Only root can run $(basename $0) or use sudo $(basename $0)"; exit 1
fi

#Remove apt cache
rm -rf $PFAD/knx/source/KNOPPIX/var/cache/apt/archives/*.deb

lsof $PFAD/knx/source/KNOPPIX/dev | grep KNOPPIX

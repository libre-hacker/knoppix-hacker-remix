#! /bin/bash
source variables.sh

if [ "0$UID" -ne 0 ]; then
   echo "Error : Only root can run $(basename $0) or use sudo $(basename $0)"; exit 1
fi

# Update the file hashes used by the "testcd" boot option
echo "Create Sha1 sum for content"
cd $PFAD/knx/master ; find -type f -not -name \
  sha1sums -not -name boot.cat -not \
  -name isolinux.bin -exec sha1sum '{}' \; > KNOPPIX/sha1sums


#!/bin/bash
# Path to partition where this remaster script are stored
PFAD="/var/knoppix/"

# Path to iso to remaster
path_iso="../KNOPPIX_V8.6.1-2019-10-14-EN.iso"

# Distrib Name
distrib_name="HackerOS"

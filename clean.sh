#!/bin/bash
source variables.sh

if [ "0$UID" -ne 0 ]; then
   echo "Error : Only root can run $(basename $0) or use sudo $(basename $0)"; exit 1
fi

#reset directory
if [ -d $PFAD/knx ]
then
    echo "reset $PFAD/knx directory"
    rm -rf $PFAD/knx
fi

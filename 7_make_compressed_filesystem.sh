#! /bin/bash
source variables.sh

if [ "0$UID" -ne 0 ]; then
   echo "Error : Only root can run $(basename $0) or use sudo $(basename $0)"; exit 1
fi

#copy tools
cp *_compressed_fs $PFAD/

echo "Make all compressed filesystem"
for i in `ls $PFAD/knx/source/ | grep KNOPPIX`
do
if [ ! -f $PFAD/knx/master/KNOPPIX/$i ]
then
    # Make the big compressed filesystem KNOPPIX
    echo "Make compressed filesystem $i"
    genisoimage -input-charset ISO-8859-15 -R -l -D -V KNOPPIX_FS -quiet \
      -no-split-symlink-components -no-split-symlink-fields \
      -hide-rr-moved -cache-inodes $PFAD/knx/source/$i \
      | $PFAD/create_compressed_fs -q -B 65536 -t 8 -L 9 \
      -f $PFAD/knx/isotemp - $PFAD/knx/master/KNOPPIX/$i
fi
done


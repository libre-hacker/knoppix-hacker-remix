#!/bin/bash
source variables.sh

if [ "0$UID" -ne 0 ]; then
   echo "Error : Only root can run $(basename $0) or use sudo $(basename $0)"; exit 1
fi

# To use the Internet add your nameserver into the chroot folder
cp /etc/resolv.conf $PFAD/knx/source/KNOPPIX/etc/resolv.conf
# Allow X-based programs in chroot
[ -e $PFAD/knx/source/KNOPPIX/home/knoppix/.Xauthority ] \
   && rm $PFAD/knx/source/KNOPPIX/home/knoppix/.Xauthority
if [ -f /home/knoppix/.Xauthority ]
then
	cp /home/knoppix/.Xauthority $PFAD/knx/source/KNOPPIX/home/knoppix
	chown knoppix:knoppix $PFAD/knx/source/KNOPPIX/home/knoppix/.Xauthority
fi
# prepare enviroment for chroot
mount --bind /dev $PFAD/knx/source/KNOPPIX/dev
mount -t proc proc $PFAD/knx/source/KNOPPIX/proc
mount -t sysfs sysfs $PFAD/knx/source/KNOPPIX/sys
mount --bind /dev/pts $PFAD/knx/source/KNOPPIX/dev/pts
mount --bind /tmp $PFAD/knx/source/KNOPPIX/tmp

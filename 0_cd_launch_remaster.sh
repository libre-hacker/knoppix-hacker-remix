#!/bin/bash
source variables.sh

if [ "0$UID" -ne 0 ]; then
   echo "Error : Only root can run $(basename $0) or use sudo $(basename $0)"; exit 1
fi

# mount iso in $PFAD/isocd
if [ ! -d $PFAD/isocd/boot/ ]
then
    ./00_mount_iso.sh
fi

START=$(date +'%s')
# Disable screensaver
if [ `which xscreensaver-command` ]
then
    xscreensaver-command -exit
fi

# REQUIREMENTS
#sudo apt-get install zlib1g:i386
#apt install lib32z1
#apt-cache search zlib1g
#sudo apt-get install zlib1g
#sudo apt-get install zlib1g-dev

# One sub-directory will be used for the Master-CD
mkdir -p $PFAD/knx/master

if [ -f /usr/bin/extract_compressed_fs ]
then
    cp /usr/bin/extract_compressed_fs $PFAD/
    cp /usr/bin/create_compressed_fs $PFAD/
else
    cp ./extract_compressed_fs $PFAD/extract_compressed_fs
    cp ./create_compressed_fs $PFAD/
fi

cd $PFAD/knx

# You will need a swapfile
if [ ! -f "swapfile" ]
then
    echo "Make swapfile"
    dd if=/dev/zero of=swapfile bs=1M count=500
    mkswap swapfile ; swapon swapfile
fi

if [ ! -f "$PFAD/source_cd/knoppix2.iso" ]
then
    if [ ! -d $PFAD/source_cd/ ]
    then
        mkdir -p $PFAD/source_cd/
    fi

    echo "Extract KNOPPIX squashfs"

    $PFAD/extract_compressed_fs $PFAD/isocd/KNOPPIX/KNOPPIX  $PFAD/source_cd/knoppix.iso
    echo "Extract KNOPPIX1 squashfs"
    $PFAD/extract_compressed_fs $PFAD/isocd/KNOPPIX/KNOPPIX1  $PFAD/source_cd/knoppix1.iso
    echo "Extract KNOPPIX2 squashfs"
    $PFAD/extract_compressed_fs $PFAD/isocd/KNOPPIX/KNOPPIX2  $PFAD/source_cd/knoppix2.iso

    if [ ! -d "$PFAD/knoppix/" ]
    then
        mkdir -p $PFAD/knoppix/
    fi
    mount -o loop $PFAD/source_cd/knoppix.iso $PFAD/knoppix/

    if [ ! -d "$PFAD/knoppix/1" ]
    then
        mkdir -p $PFAD/knoppix1/
    fi
    mount -o loop $PFAD/source_cd/knoppix1.iso $PFAD/knoppix1/

    if [ ! -d "$PFAD/knoppix/2" ]
    then
        mkdir -p $PFAD/knoppix2/
    fi
    mount -o loop $PFAD/source_cd/knoppix2.iso $PFAD/knoppix2/
else
    echo "knoppix.iso exist, mount in tmp"
    mkdir -p $PFAD/knoppix/
    mount -o loop $PFAD/source_cd/knoppix.iso $PFAD/knoppix/
    mkdir -p $PFAD/knoppix1/
    mount -o loop $PFAD/source_cd/knoppix1.iso $PFAD/knoppix1/
    mkdir -p $PFAD/knoppix2/
    mount -o loop $PFAD/source_cd/knoppix2.iso $PFAD/knoppix2/
fi

echo "Copy the KNOPPIX files to your source directory."
echo "This will take a long time!"
mkdir -p $PFAD/knx/source/KNOPPIX
if [ ! -d "$PFAD/knx/source/KNOPPIX/var" ]
then
    cp -rp $PFAD/knoppix/* $PFAD/knx/source/KNOPPIX/
fi
if [ ! -d "$PFAD/knx/source/KNOPPIX1/var" ]
then
    mkdir -p $PFAD/knx/source/KNOPPIX1
    cp -rp $PFAD/knoppix1/* $PFAD/knx/source/KNOPPIX1/
fi
if [ ! -d "$PFAD/knx/source/KNOPPIX2/opt" ]
then
    mkdir -p $PFAD/knx/source/KNOPPIX2
    cp -rp $PFAD/knoppix2/* $PFAD/knx/source/KNOPPIX2/
fi

# Additionally, copy the files to build the ISO later
echo "Rsync iso without KNOPPIX* compressed files in master"
rsync -aH --exclude="KNOPPIX/KNOPPIX*" $PFAD/isocd/* $PFAD/knx/master
# gunzip inital RAM-disk
mkdir -p $PFAD/knx/minirt/minirtdir
cp $PFAD/isocd/boot/isolinux/minirt.gz $PFAD/knx/minirt/
cd $PFAD/knx/minirt/
gunzip minirt.gz
cd minirtdir
cpio -imd --no-absolute-filenames < ../minirt

# Enable screensaver
knoppix_user=`cat /etc/passwd | grep knoppix`
if [ "${knoppix_user}" ]
then
    su knoppix -c "xscreensaver -nosplash &"
fi

echo -e "\nFinished! Used time: $(expr $(expr $(date +'%s') - $START) / 60) min. \
  and $(expr $(expr $(date +'%s') - $START) % 60) sec."
